package puc.tcc.logistica.geraJwtToken.visao;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import puc.tcc.logistica.geraJwtToken.dados.IUsuarioDados;
import puc.tcc.logistica.geraJwtToken.dominio.Credenciais;
import puc.tcc.logistica.geraJwtToken.dominio.Usuario;
import puc.tcc.logistica.geraJwtToken.servicos.ServicosDeUsuario;
import puc.tcc.logistica.geraJwtToken.util.DecodeUtil;

@RestController
@RequestMapping("/auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthControlador {

	@Autowired
	private ServicosDeUsuario servicosDeUsuario;
	
    /*private final String USERNAME = "c.01_tropical";
    private final String PASSWORD = "ZWRnZWNsaTplZGdlY2xpc2VjcmV0";*/
	
	@PostMapping("/login")
	public Response login(@RequestParam(name="usuario")String usuario, @RequestParam(name="senha") String senha) {
		
		Usuario u = servicosDeUsuario.findByUsernameAndSenha(usuario, senha);
		
		if(u != null) {
        	Credenciais credenciais = new Credenciais();
        	credenciais.setToken(DecodeUtil.createToken(usuario));
        	credenciais.setRefreshToken(DecodeUtil.createRefreshToken(usuario));
            return Response.ok().entity(credenciais).build();
        }else{
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }
	
	@PostMapping("/refresh")
	public Response login(@RequestParam(name="refreshToken") String refreshToken) {
		
		String username = DecodeUtil.validaRefreshToken(refreshToken);
		Usuario u = servicosDeUsuario.findByUsername(username);
		if(u != null) {
        	Credenciais credenciais = new Credenciais();
        	credenciais.setToken(DecodeUtil.createToken(username));
        	credenciais.setRefreshToken(refreshToken);
            return Response.ok().entity(credenciais).build();
        }else{
            return Response.status(HttpServletResponse.SC_UNAUTHORIZED, "Token not valid.").build();
        }
    }
	
	@PostMapping("/validaToken")
	public Response valida(@RequestParam(name="token") String token) {
		
		String username = DecodeUtil.validaToken(token);
		Usuario u = servicosDeUsuario.findByUsername(username);
		if(u != null) {
            return Response.ok().entity(username).build();
        }else{
            return Response.status(HttpServletResponse.SC_UNAUTHORIZED, "Token not valid.").build();
        }
    }

}
