package puc.tcc.logistica.geraJwtToken.util;

import java.util.Date;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class DecodeUtil {
	
	private static String key = "AIzaSyD8tNH0Tmk4BwKX6f3jvJPW4mioySRsl6";
	private static String refreshKey = "AIzaSyD8tNH0TZWRnZWNsaTplZGdlY";
	
	public static String validaRefreshToken(String token) {

		if(token == null || token.trim().isEmpty()){
			return null;
		}

		try {
			if(token.contains("Bearer ")) {
				token = token.replace("Bearer ", "");
			}
			Jws<Claims> parser = decodeRefresh(token);
			return parser.getBody().getSubject();
		} catch (Exception e) {
			return null;
		}


	}
	
	public static String validaToken(String token) {

		if(token == null || token.trim().isEmpty()){
			return null;
		}

		try {
			if(token.contains("Bearer ")) {
				token = token.replace("Bearer ", "");
			}
			Jws<Claims> parser = decodeRefresh(token);
			return parser.getBody().getSubject();
		} catch (Exception e) {
			return null;
		}


	}

	public static Jws<Claims> decode(String token){
		return Jwts.parser().setSigningKey(key).parseClaimsJws(token);
	}

	public static Jws<Claims> decodeRefresh(String token){
		return Jwts.parser().setSigningKey(refreshKey).parseClaimsJws(token);
	}

    public static String createToken(String subject) {

        return Jwts.builder()
                .setSubject(subject)
                .signWith(SignatureAlgorithm.HS512, key)
                .setExpiration(new Date(System.currentTimeMillis() + 1800000))//30 minutos
                .compact();
    }

    public static String createRefreshToken(String subject) {

        return Jwts.builder()
                .setSubject(subject)
                .signWith(SignatureAlgorithm.HS512, refreshKey)
                .setExpiration(new Date(System.currentTimeMillis() + 14400000))//4 horas
                .compact();
    }

}
