package puc.tcc.logistica.geraJwtToken.servicos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import puc.tcc.logistica.geraJwtToken.dados.IUsuarioDados;
import puc.tcc.logistica.geraJwtToken.dominio.Usuario;

@Service
public class ServicosDeUsuario {
	
	@Autowired
	private IUsuarioDados usuarioDados;
	
	public Usuario findByUsernameAndSenha(String username, String senha) {
		return usuarioDados.findByUsernameAndSenha(username, senha);
	}
	
	public Usuario findByUsername(String username) {
		return usuarioDados.findByUsername(username);
	}
	
}
