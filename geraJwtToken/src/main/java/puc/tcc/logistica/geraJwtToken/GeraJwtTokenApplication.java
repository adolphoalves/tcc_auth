package puc.tcc.logistica.geraJwtToken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication (exclude = SecurityAutoConfiguration.class)
public class GeraJwtTokenApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeraJwtTokenApplication.class, args);
	}

}
