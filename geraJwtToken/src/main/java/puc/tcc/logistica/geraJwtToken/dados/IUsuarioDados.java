package puc.tcc.logistica.geraJwtToken.dados;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import puc.tcc.logistica.geraJwtToken.dominio.Usuario;



public interface IUsuarioDados extends CrudRepository<Usuario, Long> {
	
    @Query("select u from Usuario u where u.username = ?1 and u.senha = ?2")
    Usuario findByUsernameAndSenha(String username, String senha);
    Usuario findByUsername(String username);
	
}